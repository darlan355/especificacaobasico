﻿using System.Collections.Generic;
using System.Linq;

namespace Especificacoes.Core
{
    public class Validacao<TEntidade> : IValidacao<TEntidade>
    {
        private readonly List<IRegraValidacao<TEntidade>> _regrasValidacao;

        public Validacao()
        {
            this._regrasValidacao = new List<IRegraValidacao<TEntidade>>();
        }

        protected virtual void AdicionarRegra(IRegraValidacao<TEntidade> regraValidacao)
        {
            this._regrasValidacao.Add(regraValidacao);
        }

        public List<ErroValidacao> Validar(TEntidade entidade)
        {
            var erros = new List<ErroValidacao>();
            var erroImpeditivo = false;

            foreach (var regra in this._regrasValidacao.Where(r => r.Impeditiva))
            {
                if (regra.Validar(entidade))
                    continue;

                var erroValidacao = new ErroValidacao(regra.CodigoErro, regra.MensagemErro);
                erros.Add(erroValidacao);

                if (regra.Impeditiva)
                {
                    erroImpeditivo = true;
                    break;
                }
            }

            if (!erroImpeditivo)
            {
                erros = this._regrasValidacao.Where(r => !r.Impeditiva && !r.Validar(entidade))
                    .Select(r => new ErroValidacao(r.CodigoErro, r.MensagemErro))
                    .ToList();
            }

            return erros;
        }
    }
}
