﻿namespace Especificacoes.Core
{
    public class RegraValidacao<TEntidade> : IRegraValidacao<TEntidade>
    {
        private readonly IEspecificacao<TEntidade> _especificacao;

        public string MensagemErro { get; private set; }
        public string CodigoErro { get; private set; }
        public bool Impeditiva { get; private set; }

        public RegraValidacao(IEspecificacao<TEntidade> especificacao, string mensagemErro)
        {
            this._especificacao = especificacao;
            this.MensagemErro = mensagemErro;
        }

        public RegraValidacao(IEspecificacao<TEntidade> especificacao, string mensagemErro, bool impeditiva)
            : this(especificacao, mensagemErro)
        {
            this.Impeditiva = impeditiva;
        }

        public RegraValidacao(IEspecificacao<TEntidade> especificacao, string mensagemErro, string codigoErro)
            : this(especificacao, mensagemErro)
        {
            this.CodigoErro = codigoErro;
        }

        public RegraValidacao(IEspecificacao<TEntidade> especificacao, string mensagemErro, string codigoErro,
            bool impeditiva)
            : this(especificacao, mensagemErro, codigoErro)
        {
            this.Impeditiva = impeditiva;
        }

        public bool Validar(TEntidade entidade)
        {
            return this._especificacao.EstaAtendidaPor(entidade);
        }
    }
}
