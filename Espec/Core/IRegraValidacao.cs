﻿namespace Especificacoes.Core
{
    public interface IRegraValidacao<TEntidade>
    {
        string CodigoErro { get; }
        bool Impeditiva { get; }
        string MensagemErro { get; }
        bool Validar(TEntidade entidade);
    }
}
