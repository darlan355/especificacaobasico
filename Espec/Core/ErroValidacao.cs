﻿namespace Especificacoes.Core
{
    public class ErroValidacao
    {
        public string Codigo { get; private set; }

        public string Mensagem { get; private set; }

        public ErroValidacao(string codigo, string mensagem)
        {
            this.Codigo = codigo;
            this.Mensagem = mensagem;
        }

        public ErroValidacao(string mensagem)
        {
            this.Mensagem = mensagem;
        }
    }
}
