﻿using System.Collections.Generic;

namespace Especificacoes.Core
{
    public interface IEspecificacao<TEntidade>
    {
       bool EstaAtendidaPor(TEntidade entidade);
    }
}
