﻿using System.Collections.Generic;

namespace Especificacoes.Core
{
    public interface IValidacao<in TEntidade>
    {
        List<ErroValidacao> Validar(TEntidade entidade);
    }
}
