﻿using Especificacoes.Core;

namespace Exemplo.Vendas.Validacoes.Regras
{
    public class VendaFinalizadaComItens : IEspecificacao<VendaModel>
    {
        public bool EstaAtendidaPor(VendaModel entidade)
        {
            return entidade != null && entidade.Itens != null && entidade.Itens.Count > 0;
        }
    }
}
