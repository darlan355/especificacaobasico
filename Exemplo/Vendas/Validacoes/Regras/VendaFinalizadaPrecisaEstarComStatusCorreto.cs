﻿using Especificacoes.Core;

namespace Exemplo.Vendas.Validacoes.Regras
{
    public class VendaFinalizadaPrecisaEstarComStatusCorreto : IEspecificacao<VendaModel>
    {
        // Injeção de DI se necessário
        public VendaFinalizadaPrecisaEstarComStatusCorreto()
        {

        }

        /// <summary>
        /// Aplica a regra de negócio.
        /// Se for necessário buscar na base.
        /// Tudo vai aqui
        /// <param name="entidade"></param>
        /// <returns></returns>
        public bool EstaAtendidaPor(VendaModel entidade)
        {
            return
                entidade != null &&
                entidade.Status != null &&
                entidade.Status.Equals("S");
        }
    }
}
