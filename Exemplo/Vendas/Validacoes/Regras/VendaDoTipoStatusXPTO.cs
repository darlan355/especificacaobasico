﻿using Especificacoes.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exemplo.Vendas.Validacoes.Regras
{
    public class VendaDoTipoStatusXPTO : IEspecificacao<VendaModel>
    {
        public bool EstaAtendidaPor(VendaModel entidade)
        {
            if (entidade == null || entidade.Itens == null || entidade.NomeComprador == "")
                return false;

            if (entidade.Status.Equals("P"))
                return true;

            var ok = true;

            foreach (var item in entidade.Itens)
            {
                if (item.Nome == string.Empty)
                {
                    ok = false;
                    break;
                }
            }

            return ok;
        }
    }
}
