﻿namespace Exemplo.Vendas.Validacoes
{
    public static class CodigoErro
    {
        public const string VendaNaoFinalizada = "Venda precisa estar com o status de E para ser finalizada.";
        public const string CodVendaNaoFinalizada = "000";
    }
}
