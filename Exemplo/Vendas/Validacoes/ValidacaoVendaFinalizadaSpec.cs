﻿using Especificacoes.Core;
using Exemplo.Vendas.Validacoes.Regras;

namespace Exemplo.Vendas.Validacoes
{
    public sealed class ValidacaoVendaFinalizadaSpec : Especificacoes.Core.Validacao<VendaModel>
    {
        public ValidacaoVendaFinalizadaSpec(string nomeComprador)
        {
            this.AdicionarRegra(new RegraValidacao<VendaModel>(new VendaFinalizadaPrecisaEstarComStatusCorreto(), $"Venda não foi permitida. {nomeComprador}", CodigoErro.CodVendaNaoFinalizada, false));
            this.AdicionarRegra(new RegraValidacao<VendaModel>(new VendaFinalizadaComItens(), "Venda sem itens!", CodigoErro.CodVendaNaoFinalizada, false));
            this.AdicionarRegra(new RegraValidacao<VendaModel>(new VendaDoTipoStatusXPTO(), "Venda sem itens!", CodigoErro.CodVendaNaoFinalizada, false));

        }
    }
}
