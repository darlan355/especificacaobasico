﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exemplo.Vendas
{
    public class VendaModel
    {
        
        public int Id { get; set; }
        public DateTime DataCriacao { get; set; }
        public string Status { get; set; }
        public string NomeComprador { get; set; }

        public List<ItemModel> Itens { get; set; }
    }
}
