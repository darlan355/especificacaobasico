﻿using Exemplo.Vendas.Validacoes;
using Exemplo.Vendas.Validacoes.Regras;

namespace Exemplo.Vendas
{
    public class VendaService
    {
        /// <summary>
        /// Método finalizar da service que  vai acionar as especificações
        /// </summary>
        /// <param name="venda"></param>
        /// <returns></returns>
        public bool Finalizar(VendaModel venda)
        {
            var validacao= new ValidacaoVendaFinalizadaSpec(venda.NomeComprador).Validar(venda);
            return validacao.Count.Equals(0);
        }
    }
}
